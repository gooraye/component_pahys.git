<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-23 16:40
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\pahys\context;


abstract class BaseContext
{

    private $apiId;
    private $partnerId;
    private $key;

    private $baseHttpUrl;
    private $baseHttpsUrl;

    /**
     * @return mixed
     */
    public function getApiId()
    {
        return $this->apiId;
    }

    /**
     * @param mixed $apiId
     */
    public function setApiId($apiId)
    {
        $this->apiId = $apiId;
    }

    /**
     * @return mixed
     */
    public function getPartnerId()
    {
        return $this->partnerId;
    }

    /**
     * @param mixed $partnerId
     */
    public function setPartnerId($partnerId)
    {
        $this->partnerId = $partnerId;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getBaseHttpUrl()
    {
        return $this->baseHttpUrl;
    }

    /**
     * @param mixed $baseHttpUrl
     */
    public function setBaseHttpUrl($baseHttpUrl)
    {
        $this->baseHttpUrl = $baseHttpUrl;
    }

    /**
     * @return mixed
     */
    public function getBaseHttpsUrl()
    {
        return $this->baseHttpsUrl;
    }

    /**
     * @param mixed $baseHttpsUrl
     */
    public function setBaseHttpsUrl($baseHttpsUrl)
    {
        $this->baseHttpsUrl = $baseHttpsUrl;
    }


}
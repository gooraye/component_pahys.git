<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-26 11:43
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\pahys\api;


class TestApi extends BaseApi
{
    function getApiGroup()
    {
        return "pttest_group";
    }

    function getApiName()
    {
        return "testSimpleTypeInt";
    }

    public function getVersion()
    {
        return "0.1.0";
    }

    public function index()
    {
        $salt = microtime(true);
        $url = $this->context->getBaseHttpUrl() . $this->getApiGroup() . '/' . $this->getApiName();
        $url .= '?p=' . $this->context->getPartnerId() . '&v=' . $this->getVersion() . '&s=' . $salt . '&h=';

    }
}
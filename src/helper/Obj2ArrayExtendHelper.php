<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-13 11:36
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\pahys\helper;


use by\infrastructure\helper\Object2DataArrayHelper;

class Obj2ArrayExtendHelper
{
    public static function getArrayFrom($obj)
    {
        $arr = Object2DataArrayHelper::getDataArrayFrom($obj);
        $newArr = self::convertUnderline($arr);
        return $newArr;
    }

    /**
     * @param $arr
     * @return array
     */
    public static function convertUnderline($arr)
    {
        $newArr = [];
        foreach ($arr as $key => $value) {
            $newKey = Object2DataArrayHelper::convertUnderline($key);
            if (is_array($value)) {
                $value = self::convertUnderline($value);
            }
            $newArr[$newKey] = $value;
        }
        return $newArr;
    }
}
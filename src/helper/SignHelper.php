<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-27 14:26
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace by\component\pahys\helper;


class SignHelper
{
    public static function signHmac($str, $key)
    {
        $signature = '';
        if (function_exists('hash_hmac')) {
            $signature = hash_hmac('sha1', $str, $key, true);
        } else {
            $blockSize = '64';
            $hashfunc = 'sha1';
            if (strlen($key) > $blockSize) {
                $key = pack('H*', $hashfunc($key));
            }
            $key = str_pad($key, $blockSize, chr(0X00));
            $ipad = str_repeat(chr(0X36), $blockSize);
            $opad = str_repeat(chr(0X5C), $blockSize);
            $hmac = pack('H*', $hashfunc(
                ($key ^ $opad) . pack('H*', $hashfunc(
                    ($key ^ $ipad) . $str
                ))
            ));
            $signature = $hmac;
        }
        return bin2hex($signature);
    }
}
<?php
/**
 * 注意：本内容仅限于博也公司内部传阅,禁止外泄以及用于其他的商业目的
 * @author    hebidu<346551990@qq.com>
 * @copyright 2018 www.itboye.com Boye Inc. All rights reserved.
 * @link      http://www.itboye.com/
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 * Revision History Version
 ********1.0.0********************
 * file created @ 2018-03-29 14:34
 *********************************
 ********1.0.1********************
 *
 *********************************
 */

namespace byTest\component\pahys\api;


use by\component\pahys\api\NotifyReq;
use by\component\pahys\api\OrderInfoReq;
use by\component\pahys\api\ScoreApi;
use by\component\pahys\context\DevContext;
use PHPUnit\Framework\TestCase;

class ScoreApiTest extends TestCase
{
    /**
     * @throws \ErrorException
     */
    public function call($orderNum, $bizId)
    {
        $context = new DevContext();
        $api = new ScoreApi($context);
        $apiId = "6cfacbef18dd94edec70277b2cc8430d#LATEST";
        $req = new NotifyReq();
        $req->setAppKey($context->getKey());
        $req->setTimestamp(strval(time()) . '000');
        $req->setOrderNum($orderNum);
        $req->setBizId($bizId);
        $req->setErrorMessage('');
        $req->setSuccess(true);
        $req->setSign('');
        $result = $api->queryResult($apiId, $req);
        var_dump($result);
        if ($result->isFail()) {
            var_dump(mb_convert_encoding($result->getMsg(), "gbk", "utf-8"));
        }
    }

    /**
     * @throws \ErrorException
     */
    public function testIndex()
    {
        $context = new DevContext();

        $api = new ScoreApi($context);
        $apiId = "2b5db8016db055a707063bde03b8773b#LATEST";
        $req = new OrderInfoReq();
        $req->setAppKey($context->getKey());
        $req->setActualPrice(110);
        $req->setType("type");
        $req->setUid("13818880506");
        $req->setCredits("5");
        $req->setDescription("测试");
        $req->setTimestamp(strval(time()) . '000');
        $req->setWaitAudit(false);
        $req->setItemCode("itemCode1" . time());
        $req->setOrderNum("order2" . time());
        $req->setParams('');
        $req->setFacePrice(10);
        $result = $api->dec($apiId, $req);
        var_dump($req->toArray());
        var_dump($result);
        if ($result->isFail()) {
            var_dump(mb_convert_encoding($result->getMsg(), "gbk", "utf-8"));
        } else {
            $data = $result->getData();
            $bizId = $data['bizId'];
            $result = $this->call($req->getOrderNum(), $bizId);
        }
    }
}